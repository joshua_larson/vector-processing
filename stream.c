/*
 * CIS452
 * Vector Processing
 * Josh Larson
 */


#include <pthread.h>
#include <stdio.h>
#include <errno.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <sys/wait.h>
#include <time.h>
#include <signal.h>

#define MAX 80

void* complement (void* arg);
// arguments :  arg is an untyped pointer pointing to an integer
// returns :       a pointer to whatever was passed in to arg
// side effects:  prints a greeting

void sigHandler (int);

// global (shared and specific) data

int main(int argc, char args[])
{
    pthread_t thread1;
    int status;
    char fileName[MAX];
    
    //seed random number generator
    srand(time(NULL));
    
    //install signal handler for ctrl-C
    signal (SIGINT, sigHandler);
	
    while(1){
      printf("Enter File: ");
      scanf("%s", fileName);
      //printf("%s\n", &fileName[0]);
  // create and start a thread executing the "find_file" function
  // pass each thread a pointer to its respective argument
      if ((status = pthread_create (&thread1, NULL,  find_file, &fileName[0])) != 0) {
	  fprintf (stderr, "thread create error %d: %s\n", status, strerror(status));
	  exit (1);
      }
    }
    


    return 0;
}

void* complement (void* arg)
{
    
    char *val_ptr = (char *) arg;
    char threadStr[MAX];
    
    // get B strings from input to complement
    strcpy(threadStr, val_ptr);
    
    //probability stuff...
    prob = rand()%10;
    
    if(prob < 8)
      waitTime = 1;
    else{    
      waitTime = (rand()%4) + 7;
      //printf("7-10\n");
    }
    
    
    sleep(waitTime);
    printf ("\nFile Found: %s\n", threadStr);
    pthread_exit(0);
} 

void sigHandler (int sigNum)
{
    
    if(sigNum == SIGINT){    
      double avgTime = (double)m_time / (double)m_requests;
      printf(" recieved\nNumber of requests: %d\nAverage time: %lf sec.\n", m_requests, avgTime);
      exit(0);
    }

} 
