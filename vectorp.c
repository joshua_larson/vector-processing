#include <sys/types.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#define WRITE 1
#define READ 0
#define MAX 300

/* Function to change input decimal to output binary string */

void toBin(int size, unsigned long long value, char *output){
    int i;
    output[size+1] = '\0';
    for (i = size; i >= 0; --i, value >>= 1){
        output[i] = (value & 1) + '0';
    }
}

int main(int argv, char* argc[]){
    FILE *vectora = fopen(argc[1], "r");
    FILE *vectorb = fopen(argc[2], "r");
    FILE *output = fopen("output.txt", "w");
    FILE *logfile = fopen("log.txt", "w");
    char line[MAX];
    int pipe_A[2];
    int pipe_B[2];
    unsigned long long array1[MAX];
    unsigned long long array2[MAX];
   // int array4[MAX];
   // int array5[MAX];
    unsigned long long reader, nbytes;
    pid_t pid_a, pid_b;

    
    if(argv > 3){
        fprintf(stderr, "Too many arguments.\n");
        exit(1);
    }

    if(pipe(pipe_A)){
        fprintf(stderr, "Pipe A failed.\n");
        exit(1);
    }

    if(pipe(pipe_B)){
        fprintf(stderr, "Pipe B failed.\n");
        exit(1);
    }

    /* Create initial child process (p1) */
    
    if(!(pid_a = fork())){
        /* Create child process (p0) */
        
        if(!(pid_b = fork())){
            fprintf(logfile, "p0 begins...\n\n");
            close(pipe_A[READ]); // Close unused pipe sections
            int i = 1;

            /* Grab each value from input, convert to an integer, perform a
             * bitwise NOT on each value and add the value into an array to be
             * passed to p1 */

            while (fgets(line, sizeof(line), vectorb)) {
                if (i == 1){
                array2[0] = strlen(line)-2;
                }
                fprintf(logfile, "Line %llu of Vector B: %s", i, line);
                reader = strtoull(line, NULL, 2);
                array2[i] = ~reader;
                fprintf(logfile, "Processed line (bitwise NOT): %llu\n\n", array2[i]);
                i++;
            }

            /* Write array to pipe A for p1 to grab */
            
            fprintf(logfile, "Writing to pipe A...\n\n");
            write(pipe_A[WRITE], array2, sizeof(array2));
            fprintf(logfile, "p0 exits...");
			exit(0);
        }
        
        /* This is p1.  Grab array from pipe and add 1 to each value to
         * complete bitwise complement of binary numbers */
        
        else{
			close(pipe_B[READ]); 
            int j = 1;
            nbytes = read(pipe_A[READ], array2, sizeof(array2));
            fprintf(logfile, "p1 begins...\n\n");
            
            while(array2[j]){ 
                fprintf(logfile, "Received from p0 at position %d: %llu\n",j, array2[j]);
                array2[j] += 1;
                fprintf(logfile, "Incremented value in array position %d : %llu\n\n",j, array2[j]);
                j++;
            }
            
            /* Write array to pipe B for p2 to grab */
            fprintf(logfile, "Writing to pipe B...\n\n");
            write(pipe_B[WRITE], array2, sizeof(array2));
            fprintf(logfile, "p1 exits...");
			exit(0);
        }
    }
   
	/* This is p2.  Read in Vector A and grab array from pipe for final operations */ 
    else{
        
        int k = 1;
        close(pipe_B[WRITE]);
        nbytes = read(pipe_B[READ], array2, sizeof(array2));
        char out[array2[0]];
        fprintf(logfile, "p2 begins...\n\n");
        while (fgets(line, sizeof(line), vectora)) {      
            if (k == 1){
            array1[0] = strlen(line) - 2;
            }
            fprintf(logfile, "Line %d of Vector A: %s\n", k, line);
            reader = strtoull(line, NULL, 2);
            array1[k] = reader;
            k++;
        }
        k = 1;
        while(array1[k]){
            array1[k] += array2[k];
            fprintf(logfile, "Result %llu\n", array1[k]);
            toBin(array1[0], array1[k], out);
            fprintf(output,"%s\n", out);
            k++;
        }
		exit(0);
    }
}

